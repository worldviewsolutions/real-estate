import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { RealEstate } from '../real-estate';

import { RealEstatePredictorService } from '../real-estate-predictor.service';
import { Predication } from './../predication-result';
import { FlipAnimation } from '../flip.animation';
import { Metadata } from './../metadata';
import { MetadataService } from './../metadata.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';

export interface YesNo {
  value: number;
  viewValue: string;
}
@Component({
  selector: 'app-predictor',
  templateUrl: './predictor.component.html',
  styleUrls: ['./predictor.component.css'],
  animations: FlipAnimation.animations
})
export class PredictorComponent implements OnInit {
  predictorForm: FormGroup;
  flip: string = 'inactive';
  public metadata: Metadata = {} as Metadata;
  BoolValue: YesNo[] = [
    { value: 0, viewValue: "No" },
    { value: 1, viewValue: "Yes" }
  ];
  isDone: boolean = false;
  result: Predication;
  loading: boolean;
  constructor(private fb: FormBuilder, private realEstatePredService: RealEstatePredictorService,
    private metadataService: MetadataService) {

    this.metadataService.getMetadate().subscribe(metadata => {
      this.metadata = metadata;
      console.log(this.metadata);
    });
  }

  ngOnInit() {
    this.createForm();
  }

  private createForm(): void {
    this.predictorForm = this.fb.group({
      area: ['', Validators.required],
      zip: ['', Validators.required],
      pending_Year: ['', Validators.required],
      pending_Month: ['', Validators.required],
      type: ['', Validators.required],
      subdivision: ['', Validators.required],
      high_School: ['', Validators.required],
      middle_School: ['', Validators.required],
      elementary_School: ['', Validators.required],
      new_Resale: ['', Validators.required],
      basement: ['', Validators.required],
      pool_YN: ['', Validators.required],
      garage_YN: ['', Validators.required],
      original_List_Price: ['', Validators.required],
      assd_Land: ['', Validators.required],
      bedrooms: ['', Validators.required],
      rooms: ['', Validators.required],
      fin_SF: ['', Validators.required],
      unfin_SF: ['', Validators.required],
      year_Built: ['', Validators.required],
      acres: ['', Validators.required],
      split_Foyer: [''],
      colonial: [''],
      tri_LevelQuad_Level: [''],
      custom: [''],
      style_Other: [''],
      Story_2: [''],
      cottageBungalow: [''],
      green_Certified_Home: [''],
      patio_Home: [''],
      modular: [''],
      cape: [''],
      tudor: [''],
      rowhouseTownhouse: [''],
      transitional: [''],
      ranch: [''],
      saltbox: [''],
      a_frame: [''],
      victorian: [''],
      craftsman: [''],
      mediterraneanSpanish: [''],
      dutch_Colonial: [''],
      modern: [''],
      contemporary: [''],
      farm_House: [''],
      wood: [''],
      hardboard: [''],
      brick: [''],
      synth_Stucco: [''],
      clapboard: [''],
      redwood: [''],
      asbestos: [''],
      cedar: [''],
      vinyl: [''],
      stucco: [''],
      hardiplank: [''],
      steel: [''],
      stone: [''],
      aluminum: [''],
      cedar_Shake: [''],
      shingle: [''],
      cement_Lapbox: [''],
      block: [''],
      asphalt: [''],
      siding_Other: [''],
      t111: [''],
      brick_Veneer: [''],
      singleFamilyZHVI: ['', Validators.required],
      bdrmZHVI: ['', Validators.required],
      pctHomesValIncr: ['', Validators.required],
      yrFixedMortgageAvg: ['', Validators.required],
      employementRatio: ['', Validators.required],
      monthlySupplyHouses: ['', Validators.required],
      housePriceIndex: ['', Validators.required],
      homeownerRate: ['', Validators.required],
    });
  }
  public fillUp(): void {
    this.metadataService.getdate().subscribe(
      data => {
        this.formPatch(data);
      }
    );
    console.log(this.predictorForm)
  }
  private formPatch(data: RealEstate) {
    /*data = {
      "2-Story": 1,
      "30YrFixedMortgageAvg": 4.52,
      "# Bedrooms": 5,
      "# Rooms": 9,
      "$/Fin SF": 183.11,
      "A-frame": 0,
      "Acres": 0.1894,
      "Aluminum": 0,
      "Area": "30 - Richmond",
      "Asbestos": 0,
      "Asphalt": 0,
      "Assd Land": 45000,
      "Basement (Y/N)": 1,
      "BdrmZHVI": 680900,
      "Block": 0,
      "Brick": 0,
      "Brick Veneer": 0,
      "Cape": 1,
      "Cedar": 0,
      "Cedar Shake": 0,
      "Cement Lapbox": 0,
      "Clapboard": 0,
      "Colonial": 0,
      "Contemporary": 0,
      "Cottage/Bungalow": 0,
      "Craftsman": 0,
      "Custom": 0,
      "Dutch Colonial": 0,
      "Elementary School": "Holton",
      "EmployementRatio": 60.4,
      "Farm House": 0,
      "Fin SF": 2960,
      "Garage YN": 0,
      "Green Certified Home": 0,
      "Hardboard": 0,
      "Hardiplank": 0,
      "High School": "Marshall",
      "HomeownerRate": 64.4,
      "HousePriceIndex": 430.75,
      "ListBdrmDiff": -138900,
      "Mediterranean/Spanish": 0,
      "Middle School": "Henderson",
      "Modern": 0,
      "Modular": 0,
      "MonthlySupplyHouses": 6.5,
      "New/Resale": "Resale (occupied at least once)",
      "Original List Price": 542000,
      "Patio Home": 0,
      "PctHomesValIncr": 85.48,
      "Pending Month": 8,
      "Pending Year": 2018,
      "Pool YN": 0,
      "Ranch": 0,
      "Redwood": 0,
      "Rowhouse/Townhouse": 1,
      "Saltbox": 0,
      "Shingle": 1,
      "Siding-Other": 0,
      "SingleFamilyZHVI": 211300,
      "Split Foyer": 0,
      "Steel": 0,
      "Stone": 0,
      "Stucco": 0,
      "Style-Other": 0,
      "Subdivision": "Roland Park",
      "Synth Stucco": 0,
      "T111": 0,
      "Transitional": 0,
      "Tri-Level/Quad Level": 0,
      "Tudor": 0,
      "Type": "Detached",
      "Unfin SF": 0,
      "Victorian": 0,
      "Vinyl": 1,
      "Wood": 1,
      "Year Built": 1920,
      "Zip": "23222"
    }*/
    let patchData = {
      pending_Year: data["Pending Year"],
      pending_Month: data["Pending Month"],
      area: data.Area,
      zip: data.Zip,
      type: data.Type,
      subdivision: data.Subdivision,
      high_School: data["High School"],
      middle_School: data["Middle School"],
      elementary_School: data["Elementary School"],
      new_Resale: data["New/Resale"],
      basement: data["Basement (Y/N)"],
      pool_YN: data["Pool YN"],
      garage_YN: data["Garage YN"],
      original_List_Price: data["Original List Price"],
      assd_Land: data["Assd Land"],
      bedrooms: data["# Bedrooms"],
      rooms: data["# Rooms"],
      fin_SF: data["Fin SF"],
      unfin_SF: data["Unfin SF"],
      year_Built: data["Year Built"],
      acres: data.Acres,
      singleFamilyZHVI: data.SingleFamilyZHVI,
      bdrmZHVI: data.BdrmZHVI,
      pctHomesValIncr: data.PctHomesValIncr,
      yrFixedMortgageAvg: data["30YrFixedMortgageAvg"],
      employementRatio: data.EmployementRatio,
      monthlySupplyHouses: data.MonthlySupplyHouses,
      housePriceIndex: data.HousePriceIndex,
      homeownerRate: data.HomeownerRate,
    };
    for (let style of this.metadata.Style) {
      patchData[style.controlName] = data[style.value];
    }
    for (let side of this.metadata.Siding) {
      patchData[side.controlName] = data[side.value];
    }
    this.predictorForm.patchValue(patchData);
  }

  public onSubmit(): void {
    console.log(this.predictorForm.value);
    this.isDone = !this.isDone;
    this.loading = true;
    const model: RealEstate = this.mapModel(this.predictorForm.value);
    console.log('after transformation', model);
    this.realEstatePredService.postRealEstateInfo(model)
      .subscribe((data: Predication) => {
        this.result = data;
        this.loading = false;
        console.log('result of prediction', data);
      });
  }
  private mapModel(model: any): RealEstate {
    let newModel: RealEstate = {} as RealEstate;
    for (let style of this.metadata.Style) {
      newModel[style.value] = model[style.controlName]?1:0;
    }
    for (let side of this.metadata.Siding) {
      newModel[side.value] = model[side.controlName]?1:0;
    }
    newModel['Area'] = model['area'];
    newModel['Zip'] = model['zip'];
    newModel['Type'] = model['type'];
    newModel['Subdivision'] = model['subdivision'];
    newModel["High School"] = model['high_School'];
    newModel["Middle School"] = model['middle_School'];
    newModel["Elementary School"] = model['elementary_School'];
    newModel["New/Resale"] = model['new_Resale'];
    newModel["Basement (Y/N)"] = model['basement'];
    newModel["Pool YN"] = model['pool_YN'];
    newModel["Garage YN"] = model['garage_YN'];
    newModel["Original List Price"] = model['original_List_Price'];
    newModel["Assd Land"] = model['assd_Land'];
    newModel["# Bedrooms"] = model['bedrooms'];
    newModel["# Rooms"] = model['rooms'];
    newModel["Fin SF"] = model['fin_SF'];
    newModel["Unfin SF"] = model['unfin_SF'];
    newModel["Year Built"] = model['year_Built'];
    newModel.Acres = model['acres'];
    newModel.SingleFamilyZHVI = model['singleFamilyZHVI'];
    newModel.BdrmZHVI = model['bdrmZHVI'];
    newModel.PctHomesValIncr = model['pctHomesValIncr'];
    newModel["30YrFixedMortgageAvg"] = model["yrFixedMortgageAvg"];
    newModel.EmployementRatio = model['employementRatio'];
    newModel.MonthlySupplyHouses = model['monthlySupplyHouses'];
    newModel.HousePriceIndex = model['housePriceIndex'];
    newModel.HomeownerRate = model['homeownerRate'];
    newModel["Pending Month"] = model['pending_Month'];
    newModel["Pending Year"] = model['pending_Year']
    newModel.ListBdrmDiff = this.computeListBdrmDiff(newModel);
    newModel["$/Fin SF"] = this.pricePerSquareFeet( newModel["Fin SF"], newModel["Original List Price"]);
    return newModel;
  }
  public round(decimal: number, decimalPoints: number): number {
    let roundedValue = Math.round(decimal * Math.pow(10, decimalPoints)) / Math.pow(10, decimalPoints);
    return roundedValue;
  }
  private toggleFlip(): void {
    this.flip = this.flip === 'inactive' ? 'active' : 'inactive';
  }
  public retry(): void {
    this.isDone = !this.isDone;
    this.result = null;
  }
  private computeListBdrmDiff(model: any): number {
    return model['Original List Price'] - model['BdrmZHVI'];
  }

  private pricePerSquareFeet(sf: number, homePrice: number): number {
    let pricePerSF = homePrice / sf;
    return this.round(pricePerSF, 2);
  }
}
