export interface Predication {
    pred: number;
    result: number;
}