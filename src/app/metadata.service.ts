import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import handleError from './handle-error';
import { Metadata } from './metadata';
import { catchError, map, filter } from 'rxjs/operators';
import { RealEstate } from './real-estate';

@Injectable({
  providedIn: 'root'
})
export class MetadataService {
  private metadataUrl: string = 'assets/real-estate-metadata.json';
  private dataUrl: string = 'assets/random_data.json';
  private handleError: (error: HttpErrorResponse) => Observable<never> = handleError;
  constructor(private httpClient: HttpClient) { }

  public getMetadate(): Observable<Metadata> {
    return this.httpClient.get<Metadata>(this.metadataUrl)
      .pipe(
        catchError(this.handleError)
      );
  }
  public getdate(): Observable<RealEstate> {
    return this.httpClient.get(this.dataUrl)
      .pipe(
        map(data => {
          let randomIdx = this.generateRandomIdx();
          console.log('random idx==>', randomIdx)
          return this.mapToRealEstateModel(data[randomIdx])
        }),
        catchError(this.handleError)
      );
  }
  private generateRandomIdx(): number {
    return Math.floor(Math.random() * (296 - 0) + 0);
  }
  private mapToRealEstateModel(data: any): RealEstate {
    let realEstateObj: RealEstate = {} as RealEstate;
    let objEntries = Object.entries(data);
    for (let entry of objEntries) {
      let valueIdx = entry[0].indexOf("_");
      if (valueIdx === -1) {
        realEstateObj[entry[0]] = entry[1];
      } else {
        if (entry[1] === 1) {
          let featureName = entry[0].substring(0, valueIdx);
          let featureValue = entry[0].substring(valueIdx + 1);
          realEstateObj[featureName.trim()] = featureValue.trim();
        }
      }
    }
    return realEstateObj;
  }
}
