export interface RealEstate {
    "Area": string;
    "Zip": string;
    "Type": string;
    "Pending Year": number;
    "Pending Month": number;
    "Subdivision": string;
    "High School": string;
    "Middle School": string;
    "Elementary School": string;
    "New/Resale": string;
    "Basement (Y/N)": number;
    "Pool YN": number;
    "Garage YN": number;
    "Original List Price": number;
    "Assd Land": number;
    "$/Fin SF": number;
    "# Bedrooms": number;
    "# Rooms": number;
    "Fin SF": number;
    "Unfin SF": number;
    "Year Built": number;
    "Acres": number;
    "Split Foyer": number;
    "Colonial": number;
    "Tri-Level/Quad Level": number;
    "Custom": number;
    "Style-Other": number;
    "2-Story": number;
    "Cottage/Bungalow": number;
    "Green Certified Home": number;
    "Patio Home": number;
    "Modular": number;
    "Cape": number;
    "Tudor": number;
    "Rowhouse/Townhouse": number;
    "Transitional": number;
    "Ranch": number;
    "Saltbox": number;
    "A-frame": number;
    "Victorian": number;
    "Craftsman": number;
    "Mediterranean/Spanish": number;
    "Dutch Colonial": number;
    "Modern": number;
    "Contemporary": number;
    "Farm House": number;
    "Wood": number;
    "Hardboard": number;
    "Brick": number;
    "Synth Stucco": number;
    "Clapboard": number;
    "Redwood": number;
    "Asbestos": number;
    "Cedar": number;
    "Vinyl": number;
    "Stucco": number;
    "Hardiplank": number;
    "Steel": number;
    "Stone": number;
    "Aluminum": number;
    "Cedar Shake": number;
    "Shingle": number;
    "Cement Lapbox": number;
    "Block": number;
    "Asphalt": number;
    "Siding-Other": number;
    "T111": number;
    "Brick Veneer": number;
    "SingleFamilyZHVI": number;
    "BdrmZHVI": number;
    "ListBdrmDiff": number;
    "PctHomesValIncr": number;
    "30YrFixedMortgageAvg": number;
    "EmployementRatio": number;
    "MonthlySupplyHouses": number;
    "HousePriceIndex": number;
    "HomeownerRate": number;
}
