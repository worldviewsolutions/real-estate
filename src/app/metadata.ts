interface Dropdown {
    value: string;
    controlName: string;
}
export interface Metadata {
    Area: string[];
    Type: string[];
    Zip:string[];
    Subdivision: string[];
    High_School: string[];
    Middle_School: string[];
    Elementary_School: string[];
    New_Resale: string[];
    Style: Dropdown[];
    Siding: Dropdown[];
}