import { TestBed } from '@angular/core/testing';

import { RealEstatePredictorService } from './real-estate-predictor.service';

describe('RealEstatePredictorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RealEstatePredictorService = TestBed.get(RealEstatePredictorService);
    expect(service).toBeTruthy();
  });
});
