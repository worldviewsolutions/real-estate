import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { RealEstate } from './real-estate';
import { Predication } from './predication-result';
import { catchError } from 'rxjs/operators';

import handleError from './handle-error'
@Injectable({
  providedIn: 'root'
})
export class RealEstatePredictorService {
  private lambdaApi: string = 'https://m4r5hu5jsb.execute-api.us-east-1.amazonaws.com/dev/real_estate_weekly_predictor';
  private handleError: (error: HttpErrorResponse) => Observable<never> = handleError;
  constructor(private httpClient: HttpClient) {

  }


  public postRealEstateInfo(data: RealEstate): Observable<Predication> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.httpClient.post<Predication>(this.lambdaApi, data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

}
