import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MyOwnCustomMaterialModuleModule } from './my-own-custom-material-module/my-own-custom-material-module.module';
import { HomeComponent } from './home/home.component';
import { PredictorComponent } from './predictor/predictor.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RealEstatePredictorService } from './real-estate-predictor.service';
import { HttpClientModule } from '@angular/common/http';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { ScrollingModule } from '@angular/cdk/scrolling';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PredictorComponent,
    LoadingSpinnerComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ScrollingModule,
    FlexLayoutModule,
    MyOwnCustomMaterialModuleModule,
    AppRoutingModule,
  ],
  providers: [RealEstatePredictorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
